# Welcome to the **Datathon** case.

### [+ In order to start developing a solution make a clone of the current project. +]


### This project contains everything you need to develop a solution to the current case study:

> 1. **General information** on how to solve your case. Before looking into any files here, please open the  [**_wiki_**](https://gitlab.com/datasciencesociety/case_vmware/wikis/home) section and read everything there.   
> 2. Specific information containing the **requirements** of the current case. Go to [**_'/case_study'_**](./case_study). Make sure you understand the task and if you have questions turn to the mentors of the case.   
> 3. The **datasets** which you need to solve the case. Go to [**_'/data'_**](./data).

Task background: "case_study/datathon case - power consumption v1.docx"

Basic task definition:

Given data in zf_perf_stats_10min.csv, compute VM on host assignment - dt_vm_host_assignment.csv - in the form "collected_time,vm_id,host_num" and host power state - dt_host_power_state - in the form "collected_time,host_num,host_power_state". collected_time is a unix timestamp indicating start of a 10 minute period as in zf_perf_stats_10min.csv. vm_id is a UUID of a virtual machine as referenced in zf_perf_stats_10min.csv. host_num is a number from 1 to 16 indicating one of the hosts. host_power_state is the host power state which is one of "poweredOn", "poweredOff", "poweringOn".

The computed data must obey the following restrictions:

1. A host takes 30 min to power on. 

So if a host is poweredOf at time T, it can be in state poweredOn no earlier than time T+30 min and must be in poweringOn state at time T+10 min and time T+20 min. For example, this content of dt_host_power_state is valid:

1480550400,12,poweredOff
1480551000,12,poweringOn
1480551600,12,poweringOn
1480552200,12,poweredOn
1480552800,12,poweredOff

while this is not:

1480550400,12,poweredOff
1480551000,12,poweredOn

2. Any VM can be assigned to only to a poweredOn host.

If there is an entry in dt_vm_host_assignment:

1480552200,4211880c-9402-6eb5-488a-79013bffd2c5,12
then there must be a entry in dt_host_power_state
1480552200,12,poweredOn

3. All running VMs must be assigned to hosts

Any VM present in zf_perf_stats_10min at time T must have an assignment in dt_vm_host_assignment at the same time.

4. Any host must have enough resources to host the assigned VMs.

SUM(mem.active.absolute.kiloBytes) for all VMs on the same host at a given time < 256 GB
SUM(cpu.usagemhz.rate.megaHertz) for all VMs on the same host at a given time < 30000 MHz

This rule is allowed to be violated up to 1% of the time.

5. Optimization goal: Assuming power usage is equal, minimize total time hosts are powered or powering on.

Num entries in dt_host_power_state where power_state = poweredOn or poweringOn is minimal.

